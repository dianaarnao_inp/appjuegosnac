import {Component, OnInit} from '@angular/core';
import {DeportesServService, SearchType} from '../services/deportes-serv.service';
import {Observable} from 'rxjs';
import {NavController} from '@ionic/angular';


@Component({
  selector: 'app-deportes',
  templateUrl: 'deportes.page.html',
  styleUrls: ['deportes.page.scss']
})

export class DeportesPage implements OnInit{

    deportes: any;

    results: Observable<any>;
    searchTerm: string = '';
    type: SearchType = SearchType.deporte
    url_img = 'https://www.juegosnacionales.gov.co';

  constructor(public navCtrl: NavController, private deportesService: DeportesServService) {
      this.getDeportes();

  }

    getDeportes() {
        this.deportesService.getDeportes()
            .then(data => {
                this.deportes = data;
                console.log(this.deportes);
            });
    }


  ngOnInit() {
  }

    /*searchChanged(){
      this.results = this.deportesService.searchData(this.searchTerm, this.type);

  }*/

    /*searchChanged(){
     this.results = this.deportesService.searchData(this.searchTerm);

 }*/

}

