import { Component } from '@angular/core';
import {AlertController, NavController} from '@ionic/angular';
import {version} from 'punycode';
import {EscenariosPage} from '../escenarios/escenarios.page';
import {EscenariosPageModule} from '../escenarios/escenarios.module';
import {EscenariosServService} from '../services/escenarios-serv.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mas',
  templateUrl: 'mas.page.html',
  styleUrls: ['mas.page.scss']
})
export class MasPage {
  public app_version: string;

  constructor(public alertController: AlertController, public navCtrl: NavController) {
  }

  routingEscenarios(){
    this.navCtrl.navigateForward('escenarios');
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'App Bolívar 2019',
      subHeader: 'Versión:',
      message: 'Desarrollado por Deporte Virtual INP,\r\r\r Para mayor información comunícate con nosotros',
      buttons: ['OK']
    });
    await alert.present();
    const result = await alert.onDidDismiss();
    console.log(result);
  }


}
