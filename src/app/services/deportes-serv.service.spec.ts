import { TestBed } from '@angular/core/testing';

import { DeportesServService } from './deportes-serv.service';

describe('DeportesServService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeportesServService = TestBed.get(DeportesServService);
    expect(service).toBeTruthy();
  });
});
