import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EscenariosServService {

  url = 'https://www.juegosnacionales.gov.co/sitio_dev.php/componente/escenarios/tipo/app.json';

  constructor(private  http: HttpClient) {
  }

  getEscenarios() {
    return new Promise(resolve => {
      this.http.get(this.url).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}

