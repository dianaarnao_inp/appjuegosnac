import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import {map} from 'rxjs/operators';
import {Cordova} from '@ionic-native/core';

export enum  SearchType {
  all = '',
  deporte = 'deporte',
  // series = 'series',
  // episode = 'episode'
}

@Injectable({
  providedIn: 'root'
})
export class DeportesServService {
  cordova: any;
  url = 'https://www.juegosnacionales.gov.co/sitio_dev.php/componente/resultados/tipo/app.json';
  headers = 'Access-Control-Allow-Origin' ;
  //url = 'http://www.omdbapi.com/';
  //apiKey = 'a471ec0c';
  invocation = new XMLHttpRequest();

  constructor(private  http : HttpClient) { }


  getDeportes() {
    return new Promise(resolve => {
      this.http.get(this.url).subscribe(data => {
        resolve(data), {mode: 'no-cors'};
      }, err => {
        console.log(err);
      });
    });
  }

}
