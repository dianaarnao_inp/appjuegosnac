import { TestBed } from '@angular/core/testing';

import { EscenariosServService } from './escenarios-serv.service';

describe('EscenariosServService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EscenariosServService = TestBed.get(EscenariosServService);
    expect(service).toBeTruthy();
  });
});
