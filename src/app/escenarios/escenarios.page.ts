import {Component, OnInit} from '@angular/core';
import {EscenariosServService} from '../services/escenarios-serv.service';
import {Observable} from 'rxjs';
import {NavController} from '@ionic/angular';



@Component({
  selector: 'app-escenarios',
  templateUrl: 'escenarios.page.html',
  styleUrls: ['escenarios.page.scss']
})

export class EscenariosPage implements OnInit{

    escenarios: any;

    results: Observable<any>;
    searchTerm: string ='';
    // type: SearchType = SearchType.escenarios
    url_img = 'https://www.juegosnacionales.gov.co/';

  constructor(public navCtrl: NavController, private escenariosService: EscenariosServService) {
      this.getEscenarios();

  }

    getEscenarios() {
        this.escenariosService.getEscenarios()
            .then(data => {
                this.escenarios = data;
                console.log(this.escenarios);
            });
    }


  ngOnInit() {
  }

    /*searchChanged(){
      this.results = this.deportesService.searchData(this.searchTerm, this.type);

  }*/

    /*searchChanged(){
     this.results = this.deportesService.searchData(this.searchTerm);

 }*/

}

