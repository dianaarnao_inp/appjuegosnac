import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscenariosPage } from './escenarios.page';

describe('EscenariosPage', () => {
  let component: EscenariosPage;
  let fixture: ComponentFixture<EscenariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EscenariosPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscenariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
