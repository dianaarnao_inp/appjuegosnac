import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'inicio',
        children: [
          {
            path: '',
            loadChildren: '../inicio/inicio.module#InicioPageModule'
          }
        ]
      },
      {
        path: 'calendario',
        children: [
          {
            path: '',
            loadChildren: '../calendario/calendario.module#CalendarioPageModule'
          }
        ]
      },
      {
        path: 'deportes',
        children: [
          {
            path: '',
            loadChildren: '../deportes/deportes.module#DeportesPageModule'
          }
        ]
      },
      {
        path: 'delegaciones',
        children: [
          {
            path: '',
            loadChildren: '../delegaciones/delegaciones.module#DelegacionesPageModule'
          }
        ]
      },
      {
        path: 'mas',
        children: [
          {
            path: '',
            loadChildren: '../mas/mas.module#MasPageModule'
          }
        ]
      }
    ]
  },

  {
    path: '',
    redirectTo: '/tabs/inicio',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
